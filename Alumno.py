'''
Created on 1 oct. 2020

@author: RSSpe
'''

class Alumno:
    
    def __init__(self, folio, nombre, edad, carrera, fechaInscripcion):
        self.__folio = folio
        self.nombre = nombre
        self.__edad = edad
        self.__carrera = carrera
        self.__fechaInscripcion = fechaInscripcion

    @property
    def folio(self):
        return self.__folio
    
    @property
    def edad(self):
        return self.__edad
    
    @property
    def carrera(self):
        return self.__carrera

    @property
    def fechaInscripcion(self):
        return self.__fechaInscripcion

    def __str__(self):
        return f"folio: {self.folio}, nombre: {self.nombre}, edad: {self.edad}, carrera: {self.carrera}, fechaInscripcion: {self.fechaInscripcion}"
        