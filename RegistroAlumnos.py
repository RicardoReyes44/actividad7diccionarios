'''
Created on 1 oct. 2020

@author: RSSpe
'''

from Alumno import Alumno

class RegistrarAlumnos:
    
    def __init__(self):
        self.alumnos = dict()


    def vaciarLista(self):
        print("\nLista vacia")
        self.alumnos.clear()


    def llenarLista(self):
        folio = 0
        edad = 0
        fecha = ""
        
        nombre = input("\nIntroduce nombre: ")

        while(True):
            edad = int(input("Introduce edad: "))
            
            if edad>0:
                break
            else:
                print("No puede ingresar esa edad, por favor vuelve a intentarlo")


        if len(self.alumnos)==0:
            folio = 1;
        else:
            folio = self.alumnos.get(len(self.alumnos)).folio+1


        print("\nFecha de inscripcion")
        while(True):
            dia = int(input("Introduce dia: "))
            mes = int(input("Introduce mes: "))
            año = int(input("Introduce año: "))

            if(dia>0 and dia<32 and mes>0 and mes<13 and año>0):
                fecha = f"{dia}/{mes}/{año}"
                break
            else:
                print("\nLa fecha no esta bien escrita, por favor prueba de nuevo")

        self.alumnos[folio] = Alumno(folio, nombre, edad, self.seleccionarCarrera(), fecha)


    def calcularPromedioEdades(self):
        if len(self.alumnos)==0:
            print("No hay elementos")
            return 0
        else:
            cont = 0
            
            for i in self.alumnos.values():
                cont+=i.edad
            
            return cont


    def mostrarAlumnosCarrera(self):
        if len(self.alumnos)==0:
            print("No hay elementos");
        else:
            carrera = self.seleccionarCarrera();
            cont=0;
            
            print()
            for i in self.alumnos.values():
                if carrera == i.carrera:
                    print(i)
                else:
                    cont+=1
        
            if cont==len(self.alumnos):
                print("No hay alumnos con esa carrera");


    def mostrarAlumnosInscritosDespues(self):
        print()
        if len(self.alumnos)!=0:
            cont=0
            
            for i in self.alumnos.values():
                fecha = i.fechaInscripcion.split("/")
                if int(fecha[2])>2016:
                    print(i)
                elif int(fecha[2])==2016 and int(fecha[1])>8:
                    print(i)
                elif int(fecha[2])==2016 and int(fecha[1])==8 and int(fecha[0])>10:
                    print(i)
                else:
                    cont+=1
            print("No hay elementos despues de la fecha indicada")

        else:
            print("No hay elementos")


    def seleccionarCarrera(self):
        carreras = ["ISC", "IIA", "IM", "LA", "CP"];
        
        print()
        while(True):
            print("1.- ISC");
            print("2.- IIA");
            print("3.- IM");
            print("4.- LA");
            print("5.- CP");
            opcion = int(input("Selecciona una opcion: "))
            
            if opcion<=5 and opcion>=1:
                break;
            else:
                print("Opcion inexistente, por favor vuelve a intentarlo");
        
        return carreras[opcion-1];
