'''
Created on 1 oct. 2020

@author: RSSpe
'''

from RegistroAlumnos import RegistrarAlumnos


def main():
    
    ra = RegistrarAlumnos()
    candado = True
    cont = 5
    
    while(cont>0):
        print("Debes de ingresar", cont,"alumnos para empezar")
        try:
            ra.llenarLista()
            cont-=1
            print("---------------------")
        except ValueError as error:
            print("Error en la entrada<", error, ">, por favor vuelve a intentarlo")
    
    while(candado):
        print("----------Menu principal-----------");
        print("1.- Llenar lista");
        print("2.- Vaciar lista");
        print("3.- Mostrar alumnos por carrera");
        print("4.- Calcular promedio de edades");
        print("5.- Mostrar los alumnos que se incribieron despues del (10/08/2016)");
        print("6.- Salir");

        try:
            opcion = int(input("Introduce opcion: "))

            if opcion==1:
                ra.llenarLista()

            if opcion==2:
                ra.vaciarLista()

            if opcion==3:
                ra.mostrarAlumnosCarrera()

            if opcion==4:
                print("Promedio: ", ra.calcularPromedioEdades())

            if opcion==5:
                ra.mostrarAlumnosInscritosDespues()

            if opcion==6:
                candado = False

        except ValueError as error:
            print("Error en la entrada de datos<", error ,">, por favor vuelve a intentarlo")
            
        print()
            
    print("\n----------Programa terminado----------")


if __name__ == '__main__':
    main()
